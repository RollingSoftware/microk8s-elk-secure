# Microk8s-ELK-Secure

An example project to setup a secured self signed version of ELK stack with filebeat and metric beat on microk8s

### Add elastic helm repository
```bash
sudo microk8s helm3 repo add elastic https://Helm.elastic.co
sudo microk8s helm3 repo update
```

### Create and enter namespace
```bash
sudo microk8s kubectl apply -f namespace
sudo microk8s kubectl config set-context --current --namespace=elastic
```

### Generate certs
```bash
./install-cert-manager.sh
sudo microk8s kubectl apply -f cert-issuer.yaml
sudo microk8s kubectl apply -f elasticsearch-certs.yaml
```

### Setup the Elasticsearch using helm 

```bash
sudo microk8s helm3 install elasticsearch elastic/elasticsearch -f ./elasticsearch-values.yaml
```

### Generate secrets, paste them into secrets.yaml and apply
```bash
./generate-secrets.sh
sudo microk8s kubectl apply -f secrets.yaml
```

### Setup Kibana, Filebeat, Metricbeat
```bash
sudo microk8s helm3 install kibana elastic/kibana -f kibana-values.yaml
sudo microk8s helm3 install filebeat elastic/filebeat -f filebeat-values.yaml
sudo microk8s helm3 install metricbeat elastic/metricbeat -f metricbeat-values.yaml
```
